# TP1 : The "Stack" class

## Exercises

The exercises are described [here](sujet/tp1.org). 

## Compiling

For this project, we will use [CMake](https://cmake.org/). You will
find this tool on all programming platforms (Windows, Linux, Mac OS).

To configure and compile the project:

- create a directory `build` and run the command `cmake ..`

```bash
mkdir build
cd build
cmake ..
```

The `cmake` command generates makefiles from the files 
`CMakeLists.txt` which are organised in a hierarchy

- To compile, go inside directory `build` and run command `make`

- To test, go inside directory  `build` and run command `make test`

- The `main` exec program is located in directory `build/src/`

It is also possible to configure your preferred IDE to use CMake. For example

- for VScode see [here](https://devblogs.microsoft.com/cppblog/cmake-tools-extension-for-visual-studio-code/).
- for Atom, see [here](https://atom.io/packages/build-cmake)
- etc.


## Testing

Tests are locates inside directory `test`. We will use the [Catch](https://github.com/catchorg/Catch2) library. It consists in one single file `catch.hpp` already included in the project (`test/catch.hpp`).

To run the tests, after compiling the project in directory `build`, simply type 

    make test

or run the command 

    ./test/test_stack

If a test does not work, the latter prints additional informations on the reasons for the errors. 

## Write additional tests 

It is important to **not edit** file  `test/test_main.cpp`: it contains the main() of the Catch library, and it slow to compile it. Instead, to add new tests, modify file  `test/test2.cpp`, or add new test files to the project. 

For example, you can add all the tests for the operators inside file
`test/test_operators.cpp`, and add this file to the command
`add_executable` inside file `test/CMakeLists.txt`.



