#+OPTIONS:  toc:nil ^:nil num:nil
#+language: fr

#+latex_header: \usepackage[textwidth=16cm]{geometry}
#+latex_header: \usepackage{amssymb}
#+latex_header: \newcounter{question}
#+latex_header: \newenvironment{question}{\refstepcounter{question}\par\medskip\textbf{Question~\thequestion.}~\addcontentsline{toc}{subsubsection}{\protect{Question~\thequestion}}}{$\square$ \medskip}

#+BEGIN_SRC emacs-lisp :exports none :results silent
  (setq org-latex-minted-options
        '(;;("frame" "lines")
          ;;("bgcolor" "mybg")
          ;;("fontsize" "\\scriptsize")lp
          ("mathescape" "")
          ("samepage" "")
          ("xrightmargin" "0.5cm")
          ("xleftmargin"  "0.5cm")
          ("escapeinside" "@@")
          ))
  (setq question_counter 0)
#+END_SRC

#+TITLE: Conception Objets avancée : TP 1

* Instructions

  You must add the code to solve the exercices on gitlab. You *must*
  complete a file RENDU.md containing:
  - your names
  - for every questions
    - if you solved the exercice
    - the list of unit tests that show the correctness of your solutions (where this applies).    
  

* The class  Stack

  You must desing and develop a class =Stack=. This class represents a stack data structure which contains integers. The interface is the following: 
  
  #+BEGIN_SRC c++
    class Stack {
        // data fields
    public:
        Stack();               // default constructor, empty stack
        Stack(const Stack &s); // copy constructor
        ~Stack();              // destructor

        bool isEmpty() const;  // returns true if empty
        int top() const;       // returns the element at the top
        void pop();            // removes element from the top
        void push(int elem);   // puts an element on top
        void clear();          // removes all elements
        int size() const;      // number of elements currently in the stack
        int maxsize() const;   // size of the internal representation
    };
  #+END_SRC
  
  When the stack is empty, 
  - if the user tries to call =top()=, an exception is raised;
  - if the user tries to call =pop()=, the function does nothing, and no exception is raised. 

  The class has no upper limit to the number of element that it can contain. 

  The internal representation is an array of integers of fixed size. If the stack is full and the user tries to insert a new element, 
  1) a new, larger,  array is created;
  2) the content of the old array is copied in the new array;
  3) the old array is destroyed

** Question 1: coding the class 

   Write the code of the class. Test your code with the unit tests
   that are provided in directory =test/=.  If all tests pass, when
   you do a git push, the /Continuous Integration/ icon in git-lab
   goes from red to green.


* First operator

  To easily and conveniently print the content of the stack, it is
  useful to code the following output operator:

  #+BEGIN_SRC c++
    std::ostream& operator<<(std::ostream &os, const Stack &s);  
  #+END_SRC

  We will use the operator as in the following code: 

  #+BEGIN_SRC c++
    Stack s;
    s.push(1);
    s.push(2);
    s.push(3);
    cout << s << endl;
  #+END_SRC

  and the output will be:

  : [3, 2, 1]  

  (elements are printed in the inverse order of insertion). 

** Question 2: operateur de sortie

   Write the code for the global operator
   #+BEGIN_SRC c++
     std::ostream& operator<<(std::ostream &os, const Stack &s);
   #+END_SRC
   After coding it, add a new test to the directory =test= to verify
   that it works correctly. To test this operator, you can use a
   =stringstream= as output ([[http://www.cplusplus.com/reference/sstream/stringstream/stringstream/][help]]) instead of =cout=.

** Question 3: operateur de comparaison                                   

   Write the code for the member operator:
   #+BEGIN_SRC c++
     bool operator==(const Stack &other) const;
   #+END_SRC
   with the most intuitive meaning (it returns =true= if the two
   stacks contain the same elements in the same order, and =false=
   otherwise).
   
   Write a unit test to verify the correctness of your operator. 

   Now, by using your new operators, it is easier to test the code for the stack. 


* Unit tests

** Question 4: copy constructor 

   Write additional unit tests.  In particular, you must test:
    * The correctness of the  /copy-constructor/;
    * The fact that the size of the array becomes larger automatically when you push new elements in the stack. 
    * That we always extract the elements in the inverse order of insertion. 

** Question 5: assignement operator
   Write the code for the assignement member operator: 

    #+BEGIN_SRC c++
    Stack &operator=(const Stack &other);
    #+END_SRC

    This operator must give the same result as the copy constructor. Write a test to verify this equivalence. 

** Question 6: reducing the size of the array

   Add a method =void reduce()= which reduces the size of the array to make it equal to the number of elements in the stack. 
   
   Verify the correctness of the function with new unit tests. 



* Exercice: the towers of Hanoï

  Use the class =Stack= to code the algorithm of the Hanoï towers with =n= disks. 
   
   - See the description of the problem on [[https://en.wikipedia.org/wiki/Tower_of_Hanoi][Wikipedia]].

   - Every disk is represented by an integer that is equal to its dimension. 
     The smallest disk is represented by number 1, and the larger disk is represented by number =n=

   - Every tower is represented by a stack.

   
** Question 7: safe push

   Add the following member operator to class Stack:
   #+BEGIN_SRC c++
     Stack &operator+=(int elem);
   #+END_SRC
   that performs a =push= after verifying that =elem= is smaller than
   the top of the stack, or if the stack is empty. If the top of the
   stack is smaller than =elem= than exception =IncorrectPush= is
   raised.

   Write a test for this operator. 


** Question 8: write the code 

   In file =src/main.cpp= write a program that takes as argument on
   the command line the number of disks, and shows the solution for 3
   towers on the terminal. You can use the recursive implementation of
   the algorithm which is described on Wikipedia.  In your
   implementation, you must use operator += to insert a disk on top of
   a tower.
